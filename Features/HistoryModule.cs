﻿using Carter;
using MS.AFORO255.History.Features.DTOs;
using MS.AFORO255.History.Features.Services;
using Nacos;
using System.Text.Json;

namespace MS.AFORO255.History.Features;

public class HistoryModule : ICarterModule
{
    public void AddRoutes(IEndpointRouteBuilder app)
    {
        app.MapGet("api/history/{accountId}", GetAccount)
           .Produces<HistoryResponse>()
           .Produces(StatusCodes.Status404NotFound);
    }

    private static async Task<IResult> GetAccount(int accountId, IHistoryService service, ILogger<HistoryModule> _logger)
    {
        _logger.LogInformation("Post in HistoryModule with {0}", JsonSerializer.Serialize(accountId));
        var movements = await service.GetById(accountId);
        if (movements is null)  return Results.NotFound();
        return Results.Ok(movements);
    }
}

