﻿using Carter;
using Microsoft.AspNetCore.Mvc;
using MS.AFORO255.History.Features.DTOs;
using MS.AFORO255.History.Features.Services;
using System.Security.Principal;

namespace MS.AFORO255.History.Features
{
    public class HomeModule : ICarterModule
    {
        public void AddRoutes(IEndpointRouteBuilder app)
        {
            app.MapGet("ping", Ping)
               .Produces(StatusCodes.Status404NotFound);
        }
        public IResult Ping(ILogger<HomeModule> _logger)
        {
            _logger.LogDebug("Ping ...");
            return Results.Ok(); ;
        }
    }
}
